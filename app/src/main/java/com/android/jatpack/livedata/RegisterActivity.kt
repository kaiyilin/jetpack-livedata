package com.android.jatpack.livedata

import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import kotlinx.android.synthetic.main.activity_register.*

class RegisterActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)
        register.setOnClickListener {
            ServerRequest.instance.requestRegisterAccount(name.text.toString(), pwd.text.toString())
        }

        //觀察、監聽
        //精簡這段程式碼 ： LiveData + DataBinding
        ServerRequest.instance.responseResult.observe(this, object : Observer<String> {
            override fun onChanged(str: String?) {
                result.text = str
            }

        })
    }
}