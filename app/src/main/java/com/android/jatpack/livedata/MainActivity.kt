package com.android.jatpack.livedata

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import androidx.lifecycle.Observer
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        //觀察、監聽
        //參數一：持有者MainActivity
        LoginManager.instance.liveData?.observe(this, object : Observer<String> {

            override fun onChanged(str: String?) {
                tv.text = str
            }

        })

        tv.setOnClickListener {
            //先改變
            //跳轉之前觸發一次
            LoginManager.instance.liveData.value = "CathayBank"
            startActivity(Intent(this, LoginActivity::class.java))
        }
    }

    override fun onResume() {
        super.onResume()

        Handler().postDelayed(object : Runnable {
            override fun run() {
                LoginManager.instance.liveData?.value = "kevin"
            }

        }, 3000)
    }
}