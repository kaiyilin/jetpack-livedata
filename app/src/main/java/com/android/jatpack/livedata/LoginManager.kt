package com.android.jatpack.livedata

import androidx.lifecycle.MutableLiveData

/****************************************************************
 * Copyright (C) Kevin Corporation. All rights reserved.
 *
 * Author: Kevin Lin
 * Create Date: 2020/9/13
 * Usage:
 *
 **************************************************************/
class LoginManager {

    var liveData = MutableLiveData<String>().apply { postValue("initial value")}

    private object LoginManagerHolder {
        val loginManager = LoginManager()
    }

    companion object {
        val instance = LoginManagerHolder.loginManager
    }
}