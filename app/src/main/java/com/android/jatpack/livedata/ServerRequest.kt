package com.android.jatpack.livedata

import androidx.lifecycle.MutableLiveData

/****************************************************************
 * Copyright (C) Kevin Corporation. All rights reserved.
 *
 * Author: Kevin Lin
 * Create Date: 2020/9/13
 * Usage:
 *
 **************************************************************/
class ServerRequest {

    var NAME: String = "kevin"
    var PWD: String = "123"

    //api返回結果，對外開放
    var responseResult = MutableLiveData<String>().apply { postValue("--") }

    //模擬註冊 api
    fun requestRegisterAccount(name: String?, pwd: String?) {
        if (NAME == name && PWD == pwd) {
            responseResult.value = "註冊成功！"
        } else {
            responseResult.value = "註冊失敗！"
        }
    }

    private object ServerRequestHolder {
        val serverRequest = ServerRequest()
    }

    companion object {
        val instance = ServerRequestHolder.serverRequest
    }
}