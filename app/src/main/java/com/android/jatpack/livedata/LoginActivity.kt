package com.android.jatpack.livedata

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.lifecycle.Observer
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        //一打LoginActivity 開馬上關閉，如果是其他類型的library，下面應該會引發crash
        //為什麼Livedata不會crash :livedata與lifecycle進行了關聯，所以livedata也具備生命週期監聽，也意味知道生命週期狀況，如果finish了，lifeData知道後不會再去觀察
        //finish()

        //觀察、監聽
        //後設置監聽
        LoginManager.instance.liveData.observe(this, object : Observer<String> {
            override fun onChanged(str: String?) {
                tv2.text = str + "登入頁"
                Log.e(this.javaClass.simpleName, "onChanged")
            }

        })
    }
}